#!/bin/sh
# shellcheck shell=sh # Written to comply with IEEE Std 1003.1-2017

#@ Copyright (C) 17/11/2021-EU 12:34 CEST Jacob Hrbek <kreyren@rixotstudio.cz>

# Import standard helpers
command -v die || die() { printf "FATAL: %s\\n" "$2"; exit 1 ;}
command -v einfo || einfo() { printf "INFO: %s\\n" "$1" ;}
command -v ewarn || ewarn() { printf "WARN: %s\\n" "$1" ;}
command -v okmsg || okmsg() { printf "OK: %s\\n" "$1" ;}

# Define userland
cacheDir="${XDG_CACHE_HOME:-"$HOME/.cache/"}/crate2guix"

###! Function used to cache git repository in `${XDG_CACHE_HOME:-"$HOME/.cache/"}/crate2guix/$gitRepoName`
cache-git-repo() {
	# Check if `git` is executable
	einfo "Checking if command 'git' is executable"
	if command -v git 1>/dev/null; then
		okmsg "Command 'git' is executable"
	else
	        die 3 "Command 'git' is not executable in this environment"
	fi

	# Define Arguments
	gitRepoURL="$1"
		[ -n "$gitRepoURL" ] || die 1 "Repository URL was not provided in function 'cache-git-repo'"
	gitRepoBranch="${2:-"master"}"

	# POSIX(Krey): the `sed -E` is not POSIX complying
	gitRepoName="$(printf "%s\\n" "$gitRepoURL" | sed -E "s#(https://|http://)([a-zA-Z0-9.]+)(/)([a-zA-Z0-9.-]+)(/)([a-zA-Z0-9.-]+)(.git).*#\6#g")"
	#gitRepoOwner="$(printf "%s\\n" $gitRepoURL | sed -E "s#(https://|http://)([a-zA-Z0-9.]+)(/)([a-zA-Z0-9.-]+)(/)([a-zA-Z0-9.-]+)(.git).*#\4#g")"

	einfo "Making sure that cache directory is usable"
	## Make sure that the caching dir is available
	[ -d "$cacheDir" ] || {
		einfo "Cache directory does not exists, creating a new one in '$cacheDir'"
		mkdir -p "$cacheDir" || die 1 "Unable to craete the caching directory in '$cacheDir'"
	}
	okmsg "Directory used for caching is sane"

	# Check if `git` is executable
	einfo "Checking if command 'git' is executable"
	if command -v git 1>/dev/null; then
		okmsg "Command 'git' is executable"
	else
	        die 3 "Command 'git' is not executable in this environment"
	fi

	# Cache the repo
	einfo "Caching repository '$gitRepoName'"
	[ -d "$cacheDir/$gitRepoName" ] || {
		einfo "Caching repository '$gitRepoName' in '$cacheDir/$gitRepoName'"
		git clone "$guixRepoURL" "$cacheDir/$gitRepoName" --branch "$gitRepoBranch" || case "$?" in
			1) die 1 "Failed to cache repository '$gitRepoName' in '$cacheDir/$gitRepoName'" ;;
			*) die 255 "Unknown exit code: $?"
		esac
	}
	okmsg "Repository '$gitRepoName' has been cached"

	# Make sure that the desired branch is set
	einfo "Making sure that the repository '$gitRepoName' is at branch '$gitRepoBranch'"
	[ $(git -C "$cacheDir/$gitRepoName" rev-parse --abbrev-ref HEAD) = "$gitRepoBranch" ] || {
		git -C "$cacheDir/$gitRepoName" checkout "$gitRepoBranch" --force || die 1 "Unable to checkout the desired branch in repository '$gitRepoName'"
	}
	okmsg "Branch '$gitRepoBranch' is set on repository '$gitRepoName' in '$cacheDir/$gitRepoName'"

	# Check if the cached guix repo is up-to-date
	einfo "Checking if repository '$gitRepoName' is up-to-date"
	git -C "$cacheDir/$gitRepoName" pull || die 1 "Git failed to update the cached repository '$gitRepoName'"
	okmsg "Repository '$gitRepoName' is up-to-date"
}


# DEPRECATED(Krey): Original function, set for removal
crate2guix() {
	# Define arguments and their shorthands
	package="$1"
		pkg="$package"
	version="$2"
		v="$version"
	baseBranch="master"

	einfo "Preparing to generating patch for guix package '$pkg-$v' in '$guixRepo'"

	# Check if `git` is executable
	einfo "Checking if command 'git' is executable"
	if command -v git 1>/dev/null; then
		okmsg "Command 'git' is executable"
	else
	        die 3 "Command 'git' is not executable in this environment"
	fi
	
	# Cache the repo
	## Sanity-check the cacheDir
	einfo "Managing cache directory"
	cacheDir="${XDG_CACHE_HOME:-"$HOME/.cache/"}/crate2guix"
	## Make sure that the caching dir is available
	[ -d "$cacheDir" ] || {
		einfo "Cache directory does not exists, creating a new one in '$cacheDir'"
		mkdir -p "$cacheDir" || die 1 "Unable to craete the caching directory in '$cacheDir'"
	}
	okmsg "cachedir is sane"

	## Cache the guix repo
	einfo "Caching the guix repository"
	[ -d "$cacheDir/guix" ] || {
		einfo "The guix repository is not cached, caching now"
		git clone https://git.savannah.gnu.org/git/guix.git "$cacheDir/guix" || die 1 "Unable to cache the guix repository in '$cacheDir/guix'"
	}
	okmsg "Guix repository has been cached"

	# Set the cached repo on desired branch
	einfo "Checking desired branch '$baseBranch'"
	git -C "$cacheDir/guix" checkout "$baseBranch" || case "$?" in
		*)
			echo "bwhaaa is $?"
			die 1 "checkout failed"
	esac
	okmsg "Disired branch '$baseBranch' confirmed"

	# Check if the cached guix repo is up-to-date
	einfo "Checking if cached guix repository is up-to-date"
	git -C "$cacheDir/guix" pull || die 1 "Git failed to update the cached guix repo"
	okmsg "Guix repository is up-to-date"

	# Bootstrap the cached repo
	einfo "Bootstrapping the cached repository"
	#cd "$cacheDir/guix" || die 1 "mehh"
	#"$cacheDir/guix/bootstrap" || case "$?" in
	#	127) die 1 "The used environment lacks required dependencies to build the repo" ;;
	#	*) die 255 "Unknown exit code: $?"
	#esac
	okmsg "Cached repository bootstrapped"

	# Configure the cached repo
	einfo "Configuring the cached repository"
	cd "$cacheDir/guix" || die 1 "mmehh"
	"$cacheDir/guix/configure" --localstatedir=/var || die 1 "Configuration of the guix repo failed"
	okmsg "Cached repository is configured"

	# Build the cached repo
	einfo "Building the repository"
	make -C "$cacheDir/guix" || case "$?" in
		*) die 255 "Unknown exit code: $?"
	esac
	okmsg "Cached guix repository has been built"

	# Create data directory
	einfo "Checking the data directory"
	dataDir="${XDG_DATA_HOME:-"$HOME/.local/share/"}/crate2guix"
	[ -d "$dataDir" ] || {
		mkdir -p "$dataDir" || die 1 "Unable to make a data directory in '$dataDir'"
	}
	okmsg "datadirectory is sane"

	# Provide guix repo in the data directory
	einfo "Providing the guix repository in data directory"
	[ ! -d "$dataDir/guix" ] || rm -rf "$dataDir/guix"
	cp -r "$cacheDir/guix" "$dataDir/guix" || die 1 "Unable to create data directory by copying from '$cacheDir/guix' to '$dataDir/guix'"
	okmsg "Guix repository provided in data directory"

	# Define the path to data guix dir
	guixDir="$dataDir/guix"

	# Set wrapper
	wgit="git -C $guixDir"

	# Check if provided repo is sane
	einfo "Confirming that the provided repository is sane"
	$wgit branch >/dev/null || die 1 "Path '$3' is not a valid git repository"
	okmsg "Repository is sane"

	# Get latest base branch
	#einfo "Changing branch on '$baseBranch'"
	#[ "$($wgit branch)" = "$baseBranch" ] || {
	#		$wgit checkout "$baseBranch" || case "$?" in
	#			*)
	#				echo "Bwhaaa is $?"
	#				die 1 "meh"
	#		esac
	#		$wgit pull || die 1 "meeh"			
	#}

	# Create a new branch
	einfo "Creating a new branch '$pkg-$v'"
	$wgit checkout -b "$pkg-$v" || case "$?" in
		128) # branch already exists
			$wgit branch -D "$pkg-$v" || die 1 "Unable to remove branch '$pkg-$v'"
			$wgit checkout -b "$pkg-$v" || die 1 "Unable to create the desired branch '$pkg-$v'" ;;
		*)
			echo "bwhee is $?"
			die 1 "meeeh"
	esac
	okmsg "new branch '$pkg-$v' created"

	# Import the crate
	einfo "Importing the crate in '$guixDir/gnu/packages/crates-io.scm'"
	guix import crate "$pkg@$v" >> "$guixDir/gnu/packages/crates-io.scm" || die 1 "mehhhhhh"
	okmsg "crate imported"

	cd "$guixDir" || die 1 "Unable to change directory to '$guixDir'"

	einfo "Verifying that the package is sane"
	"/$guixDir/pre-inst-env" guix build --rounds=2 "rust-$pkg-$v" | tee "$pkg-$v.build-log"
	ewarn "The environment provided by GNU Guix developers does NOT return a sane exit code -> Unable to verify whether the build was successful"

	$wgit add gnu/packages/crates-io.scm || die 1 "meeeeehhhhhh"

	$wgit commit -m "$package-$v: Generated by crate2guix"

	# Generate the patch
	$wgit show HEAD > "$pkg-$v.patch"
}

while [ "$#" -gt 0 ]; do case "$1" in
	--generate-patch) # Use the guix importer to generate the patch file
		package="$1"
			pkg="$package"
		version="$2"
			v="$version"
		targetRepo="${3:-"https://git.savannah.gnu.org/git/guix.git"}"
		targetRepoBranch="${4:-"master"}"

		cache-git-repo "$targetRepo" "$targetRepoBranch"
	;;
	--help|-h) printf "%s\\n" "Help message not defined, fixme" ;;
	*) exit 1
esac; shift; done

crate2guix shell2batch 0.4.2
